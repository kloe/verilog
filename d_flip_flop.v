module d_flip_flop(D, clock, Q, nQ);

input D, clock;
output Q, nQ;

always@(posedge clock)
begin
	Q <= D;
	nQ <= !D;
end

endmodule
