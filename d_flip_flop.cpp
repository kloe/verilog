#include "obj_dir/Vd_flip_flop.h"

#include <cassert>
#include <iostream>
#include <random>

int main() {
	std::mt19937 rng;
	std::uniform_int_distribution<char> dist(0, 1);
	Vd_flip_flop flip_flop;
	for (bool clock = false; ; clock = !clock) {
		flip_flop.clock = clock;
		flip_flop.D = dist(rng);
		flip_flop.eval();
		std::cout <<
			!!flip_flop.clock << " " <<
			!!flip_flop.D << " " <<
			!!flip_flop.Q << " " <<
			!!flip_flop.nQ << "\n";
		if (flip_flop.clock) {
			assert(flip_flop.Q == flip_flop.D);
		}
	}
	return 0;
}
